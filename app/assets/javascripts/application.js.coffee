#= require jquery
#= require jquery_ujs
#= require handlebars
#= require ember
#= require ember-data
#= require_self
#= require mapic

# for more details see: http://emberjs.com/guides/application/
window.Mapic = Ember.Application.create()

